import React from 'react';
import AddFishForm from './AddFishForm';
import EditFishForm from "./EditFishForm";
import Login from './Login';
import firebase from 'firebase';
import base, {firebaseApp} from "../base";


class Inventory extends React.Component {
    state = {
        uid: null,
        owner: null
    };

    componentDidMount() {
        firebase.auth().onAuthStateChanged(user => {
            if(user){
                this.authHandler({user});
            }
        })
    }

    authHandler = async (authData) => {
        //look up current store in firebase
        const store = await base.fetch(this.props.storeId, {context: this});
        //claim it if no owner
        if(!store.owner) {
            await base.post(`${this.props.storeId}/owner`, {
                data: authData.user.uid
            })

        }
        //set state of inventory comp to reflect current user
        this.setState({
            uid: authData.user.uid,
            owner: store.owner || authData.user.uid
        });
    };
    authenticate = (provider) => {
        const authProvider = new firebase.auth[`${provider}AuthProvider`]();
        firebaseApp.auth().signInWithPopup(authProvider).then(this.authHandler);
    };

    logout = async () => {
        await firebase.auth().signOut();
        this.setState({uid: null});
    };
    render() {
        const logout = <button onClick={this.logout}>Logout</button>

        if(!this.state.uid ) {
            return <Login authenticate={this.authenticate}/>
        }
        if(this.state.uid !== this.state.owner){
            return (
                <div>
                    <p>Sorry you are not the owner!</p>
                    {logout}
                </div>
            )
        }
    return (
        <div className="inventory">
            {logout}
            <h2>Inventory</h2>
            {Object.keys(this.props.fishes).map(key => (
                <EditFishForm
                    updateFish={this.props.updateFish}
                    key={key}
                    index={key}
                    deleteFish={this.props.deleteFish}
                    fish={this.props.fishes[key]}
                />
            ))}
        <AddFishForm addFish={this.props.addFish}/>
            <button onClick={this.props.loadSampleFishes}>
                load sample fishes
            </button>
        </div>
    );
  }
}

export default Inventory;