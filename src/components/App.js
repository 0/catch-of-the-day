import React from 'react';
import Header from './Header';
import Order from './Order';
import Inventory from './Inventory';
import sampleFishes from '../sample-fishes';
import Fish from './Fish';
import base from '../base';

class App extends React.Component {
    state = {
        fishes: {},
        order: {}
    };

    componentDidMount(){
        const {params} = this.props.match;
        //reinstate local storage
        const localStorageRef = localStorage.getItem(params.storeId);
        if(localStorageRef){
            this.setState({order: JSON.parse(localStorageRef)})
        }

        this.ref = base.syncState(`${params.storeId}/fishes`, {
            context: this,
            state: 'fishes'
        });

    }

    componentDidUpdate(){
        localStorage.setItem(this.props.match.params.storeId, JSON.stringify(this.state.order));
    }

    componentWillUnmount() {
        base.removeBinding(this.ref);
    }


    addFish = (fish) => {
        //take a copy of existing state
        const fishes = { ...this.state.fishes };
        //add new fish
        fishes[`fish${Date.now()}`] = fish;
        //set new fish object to state
        this.setState({
            fishes: fishes
        });

    };

    deleteFish = (key) => {
        const fishes = {...this.state.fishes};

        fishes[key] = null;
        this.setState({ fishes });
    };

    updateFish = (key, updatedFish) => {
        const fishes = { ...this.state.fishes};
        fishes[key] = updatedFish;
        this.setState({fishes})

    };
    loadSampleFishes = () => {
        this.setState({fishes:sampleFishes})
    };

    addToOrder = (key) => {
      const order = { ...this.state.order};
      order[key] = order[key] + 1 || 1;
      this.setState({ order });

    };

    removeFromOrder = (key) => {
        const order = { ...this.state.order};
        delete order[key];
        this.setState({order});
    };

    render(){
        return (
            <div className="catch-of-the-day">
                <div className="menu">
                    <Header tagline='Fresh Seafood Market'/>
                    <ul className="fishes">
                        {Object.keys(this.state.fishes).map(key => (
                            <Fish
                                key={key}
                                index={key}
                                details={this.state.fishes[key]}
                                addToOrder={ this.addToOrder }
                            />
                        ))}
                    </ul>
                </div>
                <Order removeFromOrder={this.removeFromOrder} fishes={this.state.fishes} order={this.state.order} />
                <Inventory
                    loadSampleFishes={this.loadSampleFishes}
                    addFish={this.addFish}
                    deleteFish={this.deleteFish}
                    updateFish={this.updateFish}
                    fishes={this.state.fishes}
                    storeId={this.props.match.params.storeId}

                />

            </div>
        )
    }
}

export default App;