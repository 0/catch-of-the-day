import React from 'react';
import PropTypes from 'prop-types'

const Login = (props) => {
    return (
    <nav className="login">
        <h2>Inventory Login</h2>
        <p>Sign in to manage stores inventory</p>
        <button onClick={() => props.authenticate('Github')} className="github">
            Login With Github
        </button>
        <button onClick={() => props.authenticate('Facebook')} className="facebook">
            Login With Facebook
        </button>
        <button onClick={() => props.authenticate('Twitter')} className="twitter">
            Login With Twitter
        </button>
    </nav>
    )
};

Login.propTypes = {
    authenticate: PropTypes.func.isRequired
};

export default Login;