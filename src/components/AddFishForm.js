import React from 'react';

class AddFishForm extends React.Component {
    nameRef = React.createRef();
    priceRef = React.createRef();
    statusRef = React.createRef();
    descRef = React.createRef();
    imageRef = React.createRef();

    createFish = (e) => {
        e.preventDefault();
        const fish = {
            name: this.nameRef.value,
            price: parseFloat(this.priceRef.value),
            status: this.statusRef.value,
            desc: this.descRef.value,
            image: this.imageRef.value
        };
        this.props.addFish(fish);

        //refresh form
        e.currentTarget.reset();
    };
    render() {
        return (
            <form className="fish-edit" onSubmit={this.createFish}>
                <input type="text" ref={e => this.nameRef = e} name="name" placeholder="Name"/>
                <input type="text" ref={e => this.priceRef = e} name="price" placeholder="Price"/>
                <select name="status" ref={e => this.statusRef = e} >
                    <option value="available">Fresh!</option>
                    <option value="unavailable">Sold Out!</option>
                </select>
                <textarea name="desc" ref={e => this.descRef = e} placeholder="Desc"/>
                <input type="text" ref={e => this.imageRef = e} name="image" placeholder="Image"/>
                <button type="submit"> + Add Fish</button>
            </form>
        );
    }
}

export default AddFishForm;